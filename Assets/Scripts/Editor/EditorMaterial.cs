﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text.RegularExpressions;

public class EditorMaterial : EditorWindow
{
    [MenuItem("Assets/Crea Cartelle Objects #a")]
    private static void CreateObjectsFolder()
    {
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        Directory.CreateDirectory(path + "/" + "dir");
        Directory.CreateDirectory(path + "/dir" + "/Model");
        Directory.CreateDirectory(path + "/dir" + "/Variants");
        Directory.CreateDirectory(path + "/dir" + "/Variants/Style/Materials");
        Directory.CreateDirectory(path + "/dir" + "/Variants/Style/Textures");
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Crea Cartelle Objects #a")]
    private static void ResizeSmoothness()
    {
        foreach (var path_texture in Directory.GetFiles(( @"\Textures")))
        { }
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/copy materials #q")]
    public static void CopyMaterialsInAllVariants()
    {
        string[] file_materials;
        string path = Application.dataPath + @"\Resources\Object";
        string name;
        foreach (var item in Directory.GetDirectories(path))
        {
            path = item + @"\Model\Materials";
            file_materials = Directory.GetFiles(path);
            if (file_materials == null || file_materials.Length == 0)
            {   //debug definitivo, non eliminare
               Debug.LogError("nessun file \"Material\" in : " + path);
                continue;
            }
            foreach (var variant in Directory.GetDirectories((item + @"\Variants")))
            {
                path = variant + @"\Materials";
                Directory.Delete(path, true);
                Directory.CreateDirectory(path);
                foreach (var material in file_materials)
                {
                    name = material.Split('\\')[material.Split('\\').Length - 1];
                    name = path + @"\" + name;
                    if (name.Split('.')[name.Split('.').Length - 1].Equals("meta"))
                    {
                        continue;
                    }
                    File.Copy(material, name);
                }
            }
        }
        AssetDatabase.Refresh();

        Debug.Log("Finito di copiare i materiali");
    }


    [MenuItem("Assets/apply textures on materials %q")]
    public static void AssignTexturesToMaterials()
    {
        string[] file_materials;
        string path = Application.dataPath + @"\Resources\Object";
        string name;
        foreach (var item in Directory.GetDirectories(path))
        {
            path = item + @"\Model\Materials";
            file_materials = Directory.GetFiles(path);
            foreach (var variant in Directory.GetDirectories((item + @"\Variants")))
            {
                path = variant + @"\Materials";
                foreach (var material in file_materials)
                {
                    name = material.Split('\\')[material.Split('\\').Length - 1];
                    name = path + @"\" + name;
                    if (name.Split('.')[name.Split('.').Length - 1].Equals("meta"))
                    {
                        continue;
                    }
                    ApplicaTexture(variant, name);
                }
            }
        }
        AssetDatabase.Refresh();
        Debug.Log("Finito di applicare le texture");
    }


    /// <summary>
    /// per un material ci imposta 4 texture
    /// </summary>
    /// <param name="variant"></param>
    /// <param name="path_material"></param>
    private static void ApplicaTexture(string variant, string path_material)
    {
        string shader_property_name, name_material, name_texture;
        Material material = Resources.Load(PathTOResource(path_material)) as Material;
        if (material == null) return;
        Texture texture;
        name_material = path_material.Split('\\')[path_material.Split('\\').Length - 1].Split('.')[0];
        foreach (var path_texture in Directory.GetFiles((variant + @"\Textures")))
        {
            if (path_texture.Split('.')[path_texture.Split('.').Length - 1].Equals("meta")) continue;
            name_texture = path_texture.Split('\\')[path_texture.Split('\\').Length - 1].Split('.')[0];
            if (Regex.IsMatch(name_texture, name_material.Substring(0, name_material.Length - 1)))
            {
                texture = Resources.Load(PathTOResource(path_texture)) as Texture;
                if (texture == null) return;
                switch (path_texture.Split('_')[path_texture.Split('_').Length - 1].Split('.')[0])
                {
                    case "AlbedoTransparency": shader_property_name = "_MainTex"; break;
                    case "AO": shader_property_name = "_OcclusionMap"; break;
                    case "MetallicSmoothness": shader_property_name = "_MetallicGlossMap"; break;
                    case "Normal": shader_property_name = "_DetailNormalMap"; break;
                    default:
                        shader_property_name = "";
                        Debug.Log("Attenzione!!! texture non applicata");
                        break;
                }
                material.SetTexture(Shader.PropertyToID(shader_property_name), texture);

                material.color = Color.white;
                //Debug.Log("applicata texture: " + texture + " al materiale: " + material);
            }
        }
    }

    private static string PathTOResource(string path)
    {
        return path.Substring(path.IndexOf("Object\\")).Split('.')[0];
    }
}